// [SECTION] Document Object Model
    // Allows us to be able to access or modify the properties of an HTML element in a web page
    // It is a standard on how to get, change, add, or delete HTML elements
    // We will focus on using DOM for managing forms

    // For selecting HTML elements, we will be using the document.querySelector
    /* 
        Syntax: document.querySelector("htmlElement")
        "document" refers to the whole page
        ".querySelector" is used to select a specific object (HTML elements) from the document (web page)
    */
    // The query selector function takes a string input that is formatted like a CSS selector when applying the styles
    // Alternatively, we can use the getElement functions to retrieve the elements
        // document.getElementById("txt-first-name")
    // However, using these functions require us to identify beforehand how we geet the elements. With querySelector, we can be flexible in how to retrieve the elements

    const txtFirstName =  document.querySelector("#txt-first-name");
    const txtLastName = document.querySelector("#txt-last-name");
    const spanFullName = document.querySelector("#span-full-name");

// [SECTION] Event Listeners
    // Whenever a user interacts with a web page, this action is considered as an event
    // Working with events is large part of creating interactivity in a web page
    // To perform an action when an event triggers, you first need to listen to it

    // The method use is "addEventListener" that takes two arguments:
        // A string identifying an event
        // And a function that the listeer will execute once the "specified event" is triggered
        // When an event occurs, an "event object" is passed to the function argument as the first parameter
    txtFirstName.addEventListener("keyup", (event) => {
        console.log(event.target); // Contains the element
        console.log(event.target.value); // Contains the actual value
        // spanFullName.innerHTML = `${txtFirstName.value}`
    });

    txtFirstName.addEventListener("keyup", () => {
        // The "innerHTML" property sets or returns the HTML content (innerHTML) of an element (div, span, etc.)
        // The ".value" property sets or returns the value of an attribute (form control elements: input, select, etc.)
        spanFullName.innerHTML = `${txtFirstName.value}`
    });

    // Creating multiple events that will trigger a same function
    const fullName = () => {
        spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}` 
    }

    txtFirstName.addEventListener("keyup", fullName);
    txtLastName.addEventListener("keyup", fullName);

    //Activity s47
    	/*
    	    1. Create an addEventListener that will "change the color" of the "spanFullName". 
    	    2. Add a "select tag" element with the options/values of red, green, and blue in your index.html.
    	    3. The values of the select tag will be use to change the color of the span with the id "spanFullName"

    	    Check the following links to solve this activity:
    	    	HTML DOM Events: https://www.w3schools.com/jsref/dom_obj_event.asp
    	    	HTML DOM Style Object: https://www.w3schools.com/jsref/dom_obj_style.asp
    	*/

    let textColor = document.querySelector("#text-color");

    /* textColor.addEventListener("change", (event) => {
        let color = event.target.value;
        spanFullName.style.color = `${color}`;
    }); */

    /* textColor.addEventListener("change", () => {
        let color = textColor.value;
        spanFullName.style.color = `${color}`;
    }); */

    const changeColor = () => {
        spanFullName.style.color = textColor.value;
    }

    textColor.addEventListener("change", changeColor);
    

